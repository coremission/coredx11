﻿#pragma once
#include "FX11ShaderEffect.h"

class DXMaterial
{
protected:
    FX11ShaderEffect* m_Effect;
    // todo: maybe it is wrong place for layout
    ID3D11InputLayout* m_InputLayout{};
public:
    virtual ~DXMaterial();
    explicit DXMaterial(FX11ShaderEffect* effect);

    void BuildInputLayout();
    void SetLayout(ID3D11DeviceContext* context) { context->IASetInputLayout(m_InputLayout); }
    void DrawCall(ID3D11DeviceContext* context, size_t size) const { m_Effect->DrawCall(context, size); }

    virtual void RefreshMaterialValues();
};
