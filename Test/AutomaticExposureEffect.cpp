﻿#include "AutomaticExposureEffect.h"

AutomaticExposureEffect::AutomaticExposureEffect():
    m_Effect(FX11ShaderEffect(L"effects/compute.fx")),
    m_1x1AccumulationTexture(DXTexture::Create(1, 1, 1, 1, DXGI_FORMAT_R16_FLOAT))
{
    m_FrameTextureVariable = m_Effect.getVariableByName("gInput")->AsShaderResource();
    m_OutputTextureVariable = m_Effect.getVariableByName("gOutput")->AsUnorderedAccessView();
    m_AccumulateLuminanceTextureVariable = m_Effect.getVariableByName("gAccumulateLuminanceTexture")->AsUnorderedAccessView();
    m_AdaptationSpeed = m_Effect.getVariableByName("adaptationSpeed")->AsScalar();
}

void AutomaticExposureEffect::Dispatch(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* inputSRV, ID3D11UnorderedAccessView* inputUAV)
{
    m_FrameTextureVariable->SetResource(inputSRV);
    m_OutputTextureVariable->SetUnorderedAccessView(inputUAV);
    m_AccumulateLuminanceTextureVariable->SetUnorderedAccessView(m_1x1AccumulationTexture.getUAV());
    m_AdaptationSpeed->SetFloat(0.007f);

    // Accumulate
    m_Effect.DispatchPass(0, deviceContext, 1, 1, 1);
    
    // How many groups do we need to dispatch to cover a row of pixels, where each
    // group covers 256 pixels (the 256 is defined in the ComputeShader).
    m_Effect.DispatchPass(1, deviceContext, 800, 600, 1);

    /*
    // todo: clean up
    // Unbind the input texture from the CS for good housekeeping.
    ID3D11ShaderResourceView* nullSRV[1] = { nullptr };
    deviceContext->CSSetShaderResources(0, 1, nullSRV);

    // Unbind output from compute shader (we are going to use this output as an input in the next pass, 
    // and a resource cannot be both an output and input at the same time.
    ID3D11UnorderedAccessView* nullUAV[1] = { nullptr };
    deviceContext->CSSetUnorderedAccessViews(0, 1, nullUAV, nullptr);

    // Disable compute shader.
    deviceContext->CSSetShader(nullptr, nullptr, 0);
    */
}
