﻿#pragma once
#include "Cubemap.h"
#include "FX11ShaderEffect.h"
#include "FullScreenQuad.h"
#include "DXMaterial.h"
#include "mesh.h"

class Cube : public DefaultGeometry
{
public:
    explicit Cube();
};

class SkyboxRenderer
{
    DXMaterial* m_Material = nullptr;
    Mesh* m_SkyMesh = nullptr;
    Cube m_Geometry;
public:
    void Initialize();
    void Render(ID3D11DeviceContext* context);
};

class SkyboxMaterial: public DXMaterial
{
    ID3DX11EffectShaderResourceVariable* m_SkyboxCubeMapVariable;
    ID3DX11EffectMatrixVariable* m_OrientationVariable;
    
    Cubemap* m_SkyCubemap = nullptr;
public:
    explicit SkyboxMaterial(FX11ShaderEffect* effect);
    void RefreshMaterialValues() override;
};
