#pragma once

#include "BaseDX11Application.h"
#include "FullScreenRenderer.h"
#include "render.hpp"

class PTDXApplication: public BaseDX11Application
{
    FX11ShaderEffect effect;
    DXMaterial material;
    FullScreenRenderer renderer;

    ID3DX11EffectShaderResourceVariable* m_TextureMap;
    ID3D11ShaderResourceView* m_DiffuseMapSRV;
public:
	PTDXApplication(HINSTANCE _hInstance, int _width, int _height);
	~PTDXApplication();

	virtual void Render() override;
};

