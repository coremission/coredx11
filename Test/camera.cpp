#include <glm/glm.hpp>
#include "render.hpp"
#include "camera.h"

void Camera::rotateCamera(float _yaw, float _pitch)
{
    constexpr float mouseSensitivity = 0.01f;
    static float yaw = 0;
    static float pitch = 0;
    
    yaw += _yaw;
    pitch += _pitch;
    
    float p = pitch * mouseSensitivity;
    float y = yaw * mouseSensitivity;

    //FPS camera: RotationX(pitch) * RotationY(yaw)
    glm::quat qPitch = glm::angleAxis(p, glm::vec3(1, 0, 0));
    glm::quat qYaw = glm::angleAxis(y, glm::vec3(0, 1, 0));
    
    glm::quat orientation = qPitch * qYaw;
    orientation = glm::normalize(orientation);
    transform.setLocalRotation(orientation);
}

void Camera::moveCamera(glm::vec3 moveAmount)
{
    float speed = 5.1f;
    transform.setLocalPosition(transform.getLocalPosition() + transform.transformDirection(moveAmount) * speed);
    //transform.setLocalPosition(transform.getLocalPosition() + moveAmount * speed);
}

void Camera::nextJitterSample()
{
    m_SampleNumber = (++m_SampleNumber) % 4;// countof(samples);
}

glm::vec2 Camera::getJitterUVs()
{
    using namespace glm;
    static vec2 samples[] = {
        /*
        vec2{ 1.0f,  0.0f },
        vec2{ 0.0f, -1.0f },
        vec2{ -1.0f,  0.0f },
        vec2{ 0.0f,  1.0f },
        */
        
        vec2{-0.125f, -0.375f },//ll
        vec2{ 0.375f, -0.125f },//lr
        vec2{ 0.125f,  0.375f },//ur
        vec2{-0.375f,  0.125f },//ul
        
    };

    constexpr float factor = 0.25f; //smoothing factor
    vec2 result = samples[m_SampleNumber];

    result[0] = factor * result.x / float(SCREEN_WIDTH);
    result[1] = factor * result.y / float(SCREEN_WIDTH);

    return result;
}

glm::mat4 Camera::getViewProjection()
{
    using namespace glm;
    mat4 ProjectionMatrix = glm::perspective(glm::radians(70.0f), SCREEN_ASPECT, 0.1f, 2000.0f);
    vec2 jitterUV = getJitterUVs();

    ProjectionMatrix[2][0] += jitterUV.x;
    ProjectionMatrix[2][1] += jitterUV.y;

    return ProjectionMatrix * transform.getLocalToWorldMatrix();
}

Camera::Camera()
    : transform(Transform{}), m_SampleNumber(0)
{
}

Camera::~Camera()
{
}
