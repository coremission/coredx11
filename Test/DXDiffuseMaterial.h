#pragma once

#include <glm/glm.hpp>

#include "DXMaterial.h"
#include "render.hpp"

class DXDiffuseMaterial: public DXMaterial
{
    // Shader resources
    ID3DX11EffectMatrixVariable* m_WorldViewProjection;
    ID3DX11EffectVectorVariable* m_CameraPosition;
    ID3DX11EffectScalarVariable* m_SunLightIntensity;

    ID3DX11EffectShaderResourceVariable* m_DiffuseMap;
    ID3D11ShaderResourceView* m_DiffuseMapSRV;

public:
    void SetDiffuceMapSRV(ID3D11ShaderResourceView* diffuseMapSRV) { m_DiffuseMapSRV = diffuseMapSRV;}
    void SetMVP(glm::mat4 viewProj) { m_WorldViewProjection->SetMatrix(reinterpret_cast<float*>(&viewProj[0][0]));}

    void RefreshMaterialValues() override;
    DXDiffuseMaterial(FX11ShaderEffect* effect);
    ~DXDiffuseMaterial();
};
