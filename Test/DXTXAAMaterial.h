﻿#pragma once
#include "DXMaterial.h"
#include "DXTexture.h"

class DXTXAAMaterial: public DXMaterial
{
    // shader variables
    ID3DX11EffectShaderResourceVariable* m_CurrentFrameTexture;
    ID3DX11EffectShaderResourceVariable* m_HistoryBufferTexture;
    ID3DX11EffectShaderResourceVariable* m_DepthTexture;
    ID3DX11EffectMatrixVariable* m_PrevVP;
    ID3DX11EffectMatrixVariable* m_CurrentVP;
    ID3DX11EffectMatrixVariable* m_CurrentInverseVP;
    ID3DX11EffectVectorVariable* m_JitterUV;
    ID3DX11EffectVectorVariable* m_ScreenTexelSize;

    DXTexture m_FirstTexture;
    DXTexture m_SecondTexture;
    DXTexture m_TempTexture; // to proper merge frame
    DXTexture* currentFrameTexture;
    DXTexture* prevFrameTexture;

    DXTexture m_FirstLinearDepth;
    DXTexture m_SecondLinearDepth;
    DXTexture* currentFrameLinearDepth;
    DXTexture* prevFrameLinearDepth;
public:
    ID3D11ShaderResourceView * getCurrentFrameSRV() { return currentFrameTexture->getSRV(); }
    ID3D11UnorderedAccessView* getCurrentFrameUAV() { return currentFrameTexture->getUAV(); }
    ID3D11RenderTargetView* getCurrentFrameRenderTargetView() { return currentFrameTexture->getRenderTargetView(); }
    ID3D11Texture2D* getCurrentFrameTexture2D() { return currentFrameTexture->getTexture2D(); }

    ID3D11ShaderResourceView* getPrevFrameSRV() { return prevFrameTexture->getSRV(); }
    ID3D11RenderTargetView* getPrevFrameRenderTargetView() { return prevFrameTexture->getRenderTargetView(); }
    ID3D11Texture2D* getPrevFrameTexture2D() { return prevFrameTexture->getTexture2D(); }
    ID3D11UnorderedAccessView* getPrevFrameUAV() { return prevFrameTexture->getUAV(); }

    ID3D11ShaderResourceView* getCurrentLinearDepthSRV() { return currentFrameLinearDepth->getSRV(); }
    ID3D11RenderTargetView* getCurrentLinearDepthView() { return currentFrameLinearDepth->getRenderTargetView(); }

    ID3D11RenderTargetView* getTempRenderTargetView() { return m_TempTexture.getRenderTargetView(); }
    ID3D11Texture2D* getTempTexture2D() { return m_TempTexture.getTexture2D(); }
    void SwapFrameTextures();

    void RefreshMaterialValues() override;
    explicit DXTXAAMaterial();
};
