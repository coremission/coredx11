#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <d3d11.h>
#include "camera.h"

constexpr unsigned int SCREEN_WIDTH = 800;
constexpr unsigned int SCREEN_HEIGHT = 600;
constexpr unsigned int MSAA_SAMPLES_COUNT = 1;
constexpr float SUN_LIGHT_INTENSITY = 1000.f;

constexpr float SCREEN_ASPECT = SCREEN_WIDTH / float(SCREEN_HEIGHT);

extern ID3D11Device* g_D3Device;
extern ID3D11DeviceContext* g_D3DContext;
extern Camera g_Camera;
using color = glm::vec4;

#define globalconst extern CONST __declspec(selectany)

namespace Colors
{
    globalconst color White = { 1.0f, 1.0f, 1.0f, 1.0f };
    globalconst color Black = { 0.0f, 0.0f, 0.0f, 1.0f };
    globalconst color Red = { 1.0f, 0.0f, 0.0f, 1.0f };
    globalconst color Green = { 0.0f, 1.0f, 0.0f, 1.0f };
    globalconst color Blue = { 0.0f, 0.0f, 1.0f, 1.0f };
    globalconst color Yellow = { 1.0f, 1.0f, 0.0f, 1.0f };
    globalconst color Cyan = { 0.0f, 1.0f, 1.0f, 1.0f };
    globalconst color Magenta = { 1.0f, 0.0f, 1.0f, 1.0f };
    globalconst color Silver = { 0.75f, 0.75f, 0.75f, 1.0f };
    globalconst color LightSteelBlue = { 0.69f, 0.77f, 0.87f, 1.0f };
}
