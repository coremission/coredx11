#include <string>
#include <d3dx11effect.h>
#include <d3dcompiler.h>

#include "FX11ShaderEffect.h"
#include "helpers.hpp"
#include "render.hpp"

FX11ShaderEffect::FX11ShaderEffect(LPCWSTR filePath)
{
    CompileEffect(filePath);
}

FX11ShaderEffect::~FX11ShaderEffect()
{
    ReleaseCOM(m_FX);
}

ID3DX11EffectVariable* FX11ShaderEffect::getVariableByName(LPCSTR name)
{
    return m_FX->GetVariableByName(name);
}

void FX11ShaderEffect::DrawCall(ID3D11DeviceContext* context, size_t size)
{
    D3DX11_TECHNIQUE_DESC techDesc{};
    m_DefaultTechnique->GetDesc(&techDesc);
    for (UINT p = 0; p < techDesc.Passes; ++p)
    {
        m_DefaultTechnique->GetPassByIndex(p)->Apply(0, context);
        context->DrawIndexed(size, 0, 0);
    }
}

void FX11ShaderEffect::Dispatch(ID3D11DeviceContext* context, size_t x, size_t y, size_t z)
{
    D3DX11_TECHNIQUE_DESC techDesc{};
    m_DefaultTechnique->GetDesc(&techDesc);
    for (UINT p = 0; p < techDesc.Passes; ++p)
    {
        DispatchPass(p, context, x, y, z);
    }
}

void FX11ShaderEffect::DispatchPass(size_t passNumber, ID3D11DeviceContext* context, size_t x, size_t y, size_t z)
{
    ID3DX11EffectPass* pass = m_DefaultTechnique->GetPassByIndex(passNumber);

    pass->Apply(0, context);
    context->Dispatch(x, y, z);
}

D3DX11_PASS_DESC FX11ShaderEffect::getPassDescription(size_t passNumber)
{
    D3DX11_PASS_DESC passDesc{};
    m_DefaultTechnique->GetPassByIndex(passNumber)->GetDesc(&passDesc);
    return passDesc;
}

void FX11ShaderEffect::CompileEffect(LPCWSTR filePath)
{
    assert(g_D3Device != nullptr);

    DWORD shaderFlags = 0;
#if defined( DEBUG ) || defined( _DEBUG )
    shaderFlags |= D3D10_SHADER_DEBUG;
    shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

    ID3D10Blob* compiledShader = nullptr;
    ID3D10Blob* compilationMsgs = nullptr;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    dwShaderFlags = 0;
    HRESULT hr = D3DX11CompileEffectFromFile(filePath, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, dwShaderFlags, 0, g_D3Device, &m_FX, &compilationMsgs);

    // compilationMsgs can store errors or warnings.
    //todo: temporaly disabled
    if (compilationMsgs != nullptr)
    {
        std::string errorMessage = static_cast<char*>(compilationMsgs->GetBufferPointer());
        int l = errorMessage.length();
        if (errorMessage.length() > 53) // contains nothing but warning X4717
            MessageBox(nullptr, errorMessage.c_str(), nullptr, 0);
        ReleaseCOM(compilationMsgs);
    }

    // Even if there are no compilationMsgs, check to make sure there were no other errors.
    if (FAILED(hr))
    {
        MessageBox(nullptr, "Couldn't compile FX ", nullptr, 0);
        return;
    }

    //HRESULT hResult = D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(),
    //	0, g_D3Device, &mFX);

    // Done with compiled shader.
    //ReleaseCOM(compiledShader);

    m_DefaultTechnique = m_FX->GetTechniqueByName("Default");
}