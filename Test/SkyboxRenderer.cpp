﻿#include "SkyboxRenderer.h"

void SkyboxRenderer::Initialize()
{
    m_Material = new SkyboxMaterial(new FX11ShaderEffect(L"d:\\!Mine\\coredx11\\Test\\effects\\skybox.fx"));
    m_SkyMesh = new Mesh(m_Geometry, m_Material);
}

void SkyboxRenderer::Render(ID3D11DeviceContext* context)
{
    m_SkyMesh->Render(context);
}

SkyboxMaterial::SkyboxMaterial(FX11ShaderEffect* effect)
    : DXMaterial(effect)
{
    m_SkyCubemap = new Cubemap();
    m_SkyCubemap->LoadFromFile(L"d:\\!Mine\\coredx11\\assets\\dds\\desertcube1024.dds");
    m_SkyboxCubeMapVariable = m_Effect->getVariableByName("gCubeMap")->AsShaderResource();
    m_OrientationVariable = m_Effect->getVariableByName("gOrientation")->AsMatrix();
}

void SkyboxMaterial::RefreshMaterialValues()
{
    m_SkyboxCubeMapVariable->SetResource(m_SkyCubemap->getCubemapSRV());

    glm::mat4 orientation = glm::scale(glm::vec3(1.0f)) * g_Camera.getOrientation();

    orientation = glm::transpose(orientation);
    m_OrientationVariable->SetMatrix(reinterpret_cast<float*>(&orientation[0][0]));
}

Cube::Cube()
    : Geometry<Vertex>("indexed_cube")
{
    using namespace glm;
    vertexBuffer =
    {
        /*
        //Pos, Color, UVs, Normal
        { glm::vec3(-1.0f, -1.0f, 0.0f), Colors::White, glm::vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(-1.0f, +1.0f, 0.0f), Colors::White, glm::vec2(0.0f, 0.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(+1.0f, +1.0f, 0.0f), Colors::White, glm::vec2(1.0f, 0.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(+1.0f, -1.0f, 0.0f), Colors::White, glm::vec2(1.0f, 1.0f), glm::vec3(0, 0, 1) },
        */
        //front
        { vec3(-1.0, -1.0, 1.0), Colors::White, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, 1.0), Colors::LightSteelBlue, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, 1.0, 1.0), Colors::Red, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, 1.0), Colors::Green, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },

        //right
        { vec3(1.0, 1.0, 1.0), Colors::Cyan, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, 1.0, -1.0), Colors::Magenta, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, -1.0), Colors::Yellow, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, 1.0), Colors::White, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },

        //back
        { vec3(-1.0, -1.0, -1.0), Colors::White, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, -1.0), Colors::Red, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, 1.0, -1.0), Colors::Blue, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, -1.0), Colors::White, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },

        //left
        { vec3(-1.0, -1.0, -1.0), Colors::Magenta, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, -1.0, 1.0), Colors::Red, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, 1.0), Colors::Green, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, -1.0), Colors::Blue, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },

        //upper
        { vec3(1.0, 1.0, 1.0), Colors::Yellow, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, 1.0), Colors::Magenta, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, 1.0, -1.0), Colors::Blue, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, 1.0, -1.0), Colors::Red, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },

        //bottom
        { vec3(-1.0, -1.0, -1.0), Colors::Red, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, -1.0), Colors::White, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(1.0, -1.0, 1.0), Colors::Green, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { vec3(-1.0, -1.0, 1.0), Colors::Blue, vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
    };

    indexBuffer = {
        0, 1, 2, 0, 2, 3,       //front
        4, 5, 6, 4, 6, 7,       //right
        8, 9, 10, 8, 10, 11,    //back
        12, 13, 14, 12, 14, 15, //left
        16, 17, 18, 16, 18, 19, //upper
        20, 21, 22, 20, 22, 23  //bottom
    };

    

    m_TrianglesCount = indexBuffer.size();
}
