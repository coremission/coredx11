#include "PTDXApplication.h"



PTDXApplication::PTDXApplication(HINSTANCE _hInstance, int _width, int _height)
	:BaseDX11Application(_hInstance, _width, _height),
    effect(FX11ShaderEffect(L"simple_color.fx")),
    material(&effect)
{
    renderer.SetMaterial(&material);

    m_TextureMap = effect.getVariableByName("gTextureMap")->AsShaderResource();
}

PTDXApplication::~PTDXApplication()
{
}

void PTDXApplication::Render()
{
    m_TextureMap->SetResource(m_DiffuseMapSRV);
    renderer.Render();
}
