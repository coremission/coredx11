#pragma once

#include "FullScreenQuad.h"
#include "Mesh.h"
#include "BaseRenderer.h"

class FullScreenRenderer: public BaseRenderer
{
	FullScreenQuad m_FSQGeometry;
	Mesh* m_FSQMesh;
	DXMaterial* m_CurrentMaterial;

public:
	FullScreenRenderer();
	~FullScreenRenderer();

    void SetMaterial(DXMaterial* material);
	void Render();
};

