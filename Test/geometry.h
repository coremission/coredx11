#pragma once
#include <vector>
#include <glm/glm.hpp>

struct Vertex
{
    glm::vec3 Pos;
    glm::vec4 Color;
    glm::vec2 UVs;
    glm::vec3 Normal;
};

template <typename VertexData>
class Geometry
{
protected:
    std::string name;
    size_t m_TrianglesCount;

public:
    const std::string& getName() { return name; }
    size_t getTrianglesCount () const { return m_TrianglesCount; }
    void   setTrienglesCount(size_t value) { m_TrianglesCount = value; }
    std::vector<VertexData> vertexBuffer;
    std::vector<size_t>     indexBuffer;
    std::vector<Geometry<VertexData>*> subGeometry;

public:
    bool isStub() const { return vertexBuffer.size() == 0; }
    const void* getVertexBufferPtr() const { return &vertexBuffer[0]; }
    size_t getVertexBufferSize() const { return vertexBuffer.size() * sizeof(VertexData); }

    const void* getIndexBufferPtr() const { return &indexBuffer[0]; }
    size_t getIndexBufferSize() const { return indexBuffer.size() * sizeof(size_t); }

    Geometry(std::string _name)
        :name(_name)
    {}

    ~Geometry()
    {}
};


using DefaultGeometry = Geometry<Vertex>;