#pragma once

#include <d3dx11effect.h>

class FX11ShaderEffect
{
    ID3DX11Effect* m_FX;
    ID3DX11EffectTechnique* m_DefaultTechnique;

public:
    ID3DX11Effect* getEffect() { return m_FX; }
    ID3DX11EffectVariable* getVariableByName(LPCSTR name);
    D3DX11_PASS_DESC getPassDescription(size_t passNumber);
    void CompileEffect(LPCWSTR filePath);
    void DrawCall(ID3D11DeviceContext* context, size_t size);
    void Dispatch(ID3D11DeviceContext* context, size_t x, size_t y, size_t z);
    void DispatchPass(size_t passNumber, ID3D11DeviceContext* context, size_t x, size_t y, size_t z);
    FX11ShaderEffect(LPCWSTR filePath);
    ~FX11ShaderEffect();
};
