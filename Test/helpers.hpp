#pragma once

#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

//
#define ReleaseCOM(x) { if(x){ x->Release(); x = nullptr; } }

#if defined(DEBUG) | defined(_DEBUG)
#ifndef CHECK_HRESULT
#define CHECK_HRESULT(hr)									      \
	{									                          \
		if(FAILED(hr))							                  \
		{								                          \
			LPWSTR output;                                        \
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM |		      \
				FORMAT_MESSAGE_IGNORE_INSERTS 	 |		          \
				FORMAT_MESSAGE_ALLOCATE_BUFFER,			          \
				NULL,						                      \
				hr,						                          \
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),	      \
				(LPTSTR) &output,		              		      \
				0,						                          \
				NULL);					                          \
			MessageBox(nullptr, (LPCSTR)output, "Error", MB_OK);  \
		}								                          \
	}
#endif
#endif


template<class T, size_t N>
constexpr size_t count_of(T(&)[N]) { return N; }