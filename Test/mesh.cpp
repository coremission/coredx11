#include "mesh.h"
#include "render.hpp"
#include "helpers.hpp"
#include <array>

Mesh::Mesh(const DefaultGeometry& geometry) :
    Mesh(geometry, nullptr)
{}

Mesh::Mesh(const DefaultGeometry& geometry, DXMaterial* material):
    trianglesCount(0),
    m_Material(material)
{
    isEmpty = geometry.isStub();
    
    if (!geometry.isStub()) {
        trianglesCount = geometry.getTrianglesCount();
        BuildGeometryBuffers(geometry.getVertexBufferPtr(), geometry.getVertexBufferSize(),
            geometry.getIndexBufferPtr(), geometry.getIndexBufferSize());
    }
}

Mesh::Mesh(const Mesh& mesh)
{
    trianglesCount = mesh.trianglesCount;
    m_Material = mesh.m_Material;

    m_VertexBuffer = mesh.m_VertexBuffer;
    m_VertexBuffer->AddRef();

    m_IndexBuffer = mesh.m_IndexBuffer;
    m_IndexBuffer->AddRef();
}

Mesh::~Mesh() {
    ReleaseCOM(m_VertexBuffer);
    ReleaseCOM(m_IndexBuffer);
}

void Mesh::BuildGeometryBuffers(const void* verticesData, UINT verticesDataSize, const void* indicesData, UINT indicesDataSize) {
    // Create vertex buffer
    D3D11_BUFFER_DESC vbd;
    vbd.Usage = D3D11_USAGE_IMMUTABLE;
    vbd.ByteWidth = verticesDataSize;
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = 0;
    vbd.MiscFlags = 0;
    vbd.StructureByteStride = 0;

    D3D11_SUBRESOURCE_DATA vinitData;
    vinitData.pSysMem = verticesData;
    
    HRESULT hResult = g_D3Device->CreateBuffer(&vbd, &vinitData, &m_VertexBuffer);
    CHECK_HRESULT(hResult);

    // Create the index buffer
    D3D11_BUFFER_DESC ibd;
    ibd.Usage = D3D11_USAGE_IMMUTABLE;
    ibd.ByteWidth = indicesDataSize;
    ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ibd.CPUAccessFlags = 0;
    ibd.MiscFlags = 0;
    ibd.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA iinitData;
    iinitData.pSysMem = indicesData;

    hResult = g_D3Device->CreateBuffer(&ibd, &iinitData, &m_IndexBuffer);
    CHECK_HRESULT(hResult);
}

void Mesh::Render(ID3D11DeviceContext* context) {
    // first render sub meshes
    
    for (Mesh* subMesh : subMeshes)
        subMesh->Render(context);

    if (isEmpty)
        return;

    m_Material->SetLayout(context);
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    UINT stride = sizeof(Vertex);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);
    context->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
    
    m_Material->RefreshMaterialValues();
    m_Material->DrawCall(context, trianglesCount);
}