#pragma once

#include <glm/glm.hpp>
#include "Transform.h"
#include <glm/gtx/quaternion.hpp>

class Camera
{
    Transform transform;
    glm::vec3 m_CameraRotation;
    glm::mat4 m_PrevVewProjection;

    size_t m_SampleNumber;
public:
    Transform& getTransform() { return transform; }
    void setPrevViewProjection(glm::mat4 value) { m_PrevVewProjection = value; }
    glm::mat4 getPrevViewProjection() { return m_PrevVewProjection; }
    glm::mat4 getOrientation() { return glm::toMat4(transform.getRotation()); }
    void rotateCamera(float yaw, float pitch);
    void moveCamera(glm::vec3 moveAmount);

    glm::mat4 getViewProjection();

    void      nextJitterSample();
    glm::vec2 getJitterUVs();

    Camera();
    ~Camera();
};

