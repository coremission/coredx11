#pragma once
#include <d3d11.h>

class DXTexture
{
private:
    ID3D11ShaderResourceView* m_TextureSRV;
    ID3D11UnorderedAccessView* m_TextureUAV;
    ID3D11Texture2D* m_Texture2D;
    ID3D11RenderTargetView* m_RenderTargetView;
    ID3D11DepthStencilView* m_DepthStencilView;
public:
    ID3D11RenderTargetView*    getRenderTargetView() { return m_RenderTargetView; }
    ID3D11DepthStencilView*    getDepthStencilView() { return m_DepthStencilView; }
    ID3D11ShaderResourceView*  getSRV() { return m_TextureSRV; }
    ID3D11UnorderedAccessView* getUAV() { return m_TextureUAV; }
    ID3D11Texture2D*          getTexture2D() { return m_Texture2D; }

    static DXTexture Create(size_t width, size_t height, size_t samplesCount, size_t mipLevelsCount = 0, DXGI_FORMAT format = DXGI_FORMAT_R16G16B16A16_FLOAT, UINT bindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS);
    static DXTexture CreateDepthStencil(size_t width, size_t height, size_t samplesCount);
    
    static ID3D11ShaderResourceView* LoadTgaTexture(ID3D11Device* pDevice, const wchar_t* pSrcFile);
    static HRESULT D3DX11CreateTextureFromFile(ID3D11Device* pDevice, const wchar_t* pSrcFile, bool generateMips, ID3D11Resource** ppTexture);

    DXTexture();
    DXTexture(const DXTexture& texture);
    ~DXTexture();
};

