﻿#pragma once
#include "FX11ShaderEffect.h"
#include "DXTexture.h"

class AutomaticExposureEffect
{
    FX11ShaderEffect m_Effect;
    DXTexture m_1x1AccumulationTexture;

    ID3DX11EffectShaderResourceVariable* m_FrameTextureVariable;
    ID3DX11EffectUnorderedAccessViewVariable* m_AccumulateLuminanceTextureVariable;
    ID3DX11EffectUnorderedAccessViewVariable* m_OutputTextureVariable;
    ID3DX11EffectScalarVariable* m_AdaptationSpeed;

public:
    AutomaticExposureEffect();
    void Dispatch(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* inputSRV, ID3D11UnorderedAccessView* inputUAV);
};
