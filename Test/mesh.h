#pragma once

#include "geometry.h"

#include <d3d11.h>
#include "DXDiffuseMaterial.h"

class Mesh {
private:
    bool isEmpty;
    size_t trianglesCount;

    ID3D11Buffer* m_VertexBuffer;
    ID3D11Buffer* m_IndexBuffer;

    DXMaterial* m_Material;

    void BuildGeometryBuffers(const void* verticesData, UINT verticesDataSize, const void* indicesData, UINT indicesDataSize);
public:
    DXMaterial * getMaterial() const { return m_Material; }
    void setMaterial(DXMaterial* material) { m_Material = material; }
    std::vector<Mesh*> subMeshes;

    Mesh(const DefaultGeometry& geometry);
    Mesh(const DefaultGeometry& geometry, DXMaterial* material);

    Mesh(const Mesh& mesh);
    ~Mesh();
    void Render(ID3D11DeviceContext* context);
};