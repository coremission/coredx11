#include "DXDiffuseMaterial.h"

DXDiffuseMaterial::DXDiffuseMaterial(FX11ShaderEffect* effect)
    : DXMaterial(effect),
      m_DiffuseMapSRV(nullptr)
{
    m_WorldViewProjection = m_Effect->getVariableByName("gWorldViewProj")->AsMatrix();
    m_CameraPosition = m_Effect->getVariableByName("gCameraPos")->AsVector();
    m_SunLightIntensity = m_Effect->getVariableByName("gSunIntensity")->AsScalar();

    m_DiffuseMap = m_Effect->getVariableByName("gDiffuseMap")->AsShaderResource();
}

void DXDiffuseMaterial::RefreshMaterialValues()
{
    // Set constants
    glm::mat4 Model = glm::mat4(1.0f);
    glm::mat4 ViewProjection = g_Camera.getViewProjection();
    glm::mat4 MVP = ViewProjection * Model;

    SetMVP(MVP);
    glm::vec3 pos = g_Camera.getTransform().getLocalPosition();
    m_CameraPosition->SetFloatVector(&pos[0]);
    m_DiffuseMap->SetResource(m_DiffuseMapSRV);

    m_SunLightIntensity->SetFloat(SUN_LIGHT_INTENSITY);
}
DXDiffuseMaterial::~DXDiffuseMaterial()
{
}
