#include "BaseDX11Application.h"
#include "helpers.hpp"
#include "render.hpp"
#include "renderdoc_app.h"
#include "camera.h"
#include <iostream>
#include <Windowsx.h>
#include "DXTXAAMaterial.h"

static BaseDX11Application* g_D3DApp = nullptr;
ID3D11Device* g_D3Device;
ID3D11DeviceContext* g_D3DContext;

RENDERDOC_API_1_1_2 *rdoc_api = nullptr;

Camera g_Camera;

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    // Forward hwnd on because we can get messages (e.g., WM_CREATE)
    // before CreateWindow returns, and thus before mhMainWnd is valid.
    return g_D3DApp->MsgProc(hwnd, msg, wParam, lParam);
}

LRESULT BaseDX11Application::MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    static bool leftMouseButtonPressed = false;

    switch (msg)
    {
        // WM_ACTIVATE is sent when the window is activated or deactivated.  
        // We pause the game when the window is deactivated and unpause it 
        // when it becomes active.  
    case WM_ACTIVATE:
        if (LOWORD(wParam) == WA_INACTIVE)
        {
            //mAppPaused = true;
            //mTimer.Stop();
        }
        else
        {
            //mAppPaused = false;
            //mTimer.Start();
        }
        return 0;
        // WM_DESTROY is sent when the window is being destroyed.
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

        // The WM_MENUCHAR message is sent when a menu is active and the user presses 
        // a key that does not correspond to any mnemonic or accelerator key. 
    case WM_MENUCHAR:
        // Don't beep when we alt-enter.
        return MAKELRESULT(0, MNC_CLOSE);
    case WM_LBUTTONDOWN:
        leftMouseButtonPressed = true;
        break;
    case WM_MBUTTONDOWN:
    case WM_RBUTTONDOWN:
        //OnMouseDown(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_LBUTTONUP:
        leftMouseButtonPressed = false;
        break;
    case WM_MBUTTONUP:
    case WM_RBUTTONUP:
        //OnMouseUp(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        return 0;
    case WM_MOUSEMOVE:
        {
            static int xPos = GET_X_LPARAM(lParam);
            static int yPos = GET_Y_LPARAM(lParam);

            if (!leftMouseButtonPressed)
            {
                xPos = GET_X_LPARAM(lParam);
                yPos = GET_Y_LPARAM(lParam);
                return 0;
            }

            int newX = GET_X_LPARAM(lParam);
            int newY = GET_Y_LPARAM(lParam);

            g_Camera.rotateCamera(float(newX - xPos), float(newY - yPos));

            xPos = newX;
            yPos = newY;
            //SetCursorPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
            return 0;
        }
    case WM_KEYDOWN:
        if ((msg == WM_KEYDOWN) && (wParam == VK_ESCAPE))
        {
            PostQuitMessage(0);
            return 0;
        }

        if ((msg == WM_KEYUP) && (wParam == VK_F12))
        {
            // When you wish to trigger the capture
            if (rdoc_api)
            {   
                //rdoc_api->TriggerCapture();
                rdoc_api->TriggerMultiFrameCapture(3);
            }
        }

        if ((msg == WM_KEYDOWN))
        {
            glm::vec3 move{ 0, 0, 0 };
            glm::vec2 rotate{ 0, 0 };

            switch (wParam)
            {
                case 'W':
                    move += glm::vec3(0, 0, 1);
                    break;
                case 'S':
                    move += glm::vec3(0, 0, -1);
                    break;
                case 'A':
                    move += glm::vec3(1, 0, 0);
                    break;
                case 'D':
                    move += glm::vec3(-1, 0, 0);
                    break;
                case 'Q':
                    move += glm::vec3(0, 1, 0);
                    break;
                case 'E':
                    move += glm::vec3(0, -1, 0);
                    break;
                case VK_DOWN:
                    g_Camera.rotateCamera(0, -1);
                    break;
                case VK_UP:
                    g_Camera.rotateCamera(0, 1);
                    break;
                case VK_LEFT:
                    g_Camera.rotateCamera(-1, 0);
                    break;
                case VK_RIGHT:
                    g_Camera.rotateCamera(1, 0);
                    break;
            }
            g_Camera.moveCamera(move);
        }
        return 0;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

bool BaseDX11Application::CreateGameWindow() {
    WNDCLASSEX wc;
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = MainWndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = "D3DWindowClassName";
    wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);

    if (!RegisterClassEx(&wc))
    {
        MessageBox(nullptr, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
        return false;
    }

    hWnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        "D3DWindowClassName",
        "DX 11 Demo",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, m_Width, m_Height,
        nullptr, nullptr, hInstance, nullptr);

    if (hWnd == nullptr)
    {
        MessageBox(nullptr, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
        return false;
    }

    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

    return true;
}

bool BaseDX11Application::Init() {
    g_D3DApp = this;

    if (!CreateGameWindow())
        return false;

    // At init, on windows
    // this works only for x64 (not x86)
    HINSTANCE hGetProcIDDLL = LoadLibrary(R"(D:\!Mine\RenderDoc_1.0_32\renderdoc.dll)");

    if (!hGetProcIDDLL) {
        std::cerr << "Couldn't load Render Doc dll";
        DWORD errorCode = GetLastError();
        return false;
    }
    
    if (HMODULE mod = GetModuleHandleA("renderdoc.dll"))
    {
        pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)GetProcAddress(mod, "RENDERDOC_GetAPI");
        int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, (void **)&rdoc_api);
        assert(ret == 1);
    }

    UINT createDeviceFlags = D3D11_CREATE_DEVICE_DEBUG;

#if DEBUG || _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_FEATURE_LEVEL featureLevel;

    HRESULT hResult = D3D11CreateDevice(
        nullptr, // default display adapter
        D3D_DRIVER_TYPE_HARDWARE,
        nullptr, // no software device
        createDeviceFlags,
        nullptr, // no feature levels array
        0,
        D3D11_SDK_VERSION,
        &m_d3dDevice,
        &featureLevel,
        &m_D3DContext
    );

    if (FAILED(hResult) || featureLevel != D3D_FEATURE_LEVEL_11_0) {
        MessageBox(nullptr, "Fail to create device",
            "Or Feature level was lower than 11_0",
            MB_ICONEXCLAMATION | MB_OK);
        return false;
    }

    g_D3Device = m_d3dDevice;
    g_D3DContext = m_D3DContext;

    hResult = m_D3DContext->QueryInterface(__uuidof(m_PerfomanceAnotator), reinterpret_cast<void**>(&m_PerfomanceAnotator));
    if (FAILED(hResult))
    {
        return false;
    }

    UINT m4xMsaaQuality;
    hResult = m_d3dDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_B8G8R8A8_UNORM, MSAA_SAMPLES_COUNT, &m4xMsaaQuality);

    if (m4xMsaaQuality <= 0) {
        return false;
    }

    DXGI_SWAP_CHAIN_DESC sd{};
    sd.BufferDesc.Width = m_Width;
    sd.BufferDesc.Height = m_Height;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    sd.SampleDesc.Count = MSAA_SAMPLES_COUNT;
    sd.SampleDesc.Quality = m4xMsaaQuality - 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_UNORDERED_ACCESS;
    sd.BufferCount = 1;
    sd.OutputWindow = hWnd;
    sd.Windowed = true;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    sd.Flags = 0;

    // To correctly create the swap chain, we must use the IDXGIFactory that was
    // used to create the device.  If we tried to use a different IDXGIFactory instance
    // (by calling CreateDXGIFactory), we get an error: "IDXGIFactory::CreateSwapChain: 
    // This function is being called with a device from a different IDXGIFactory."
    IDXGIDevice* dxgiDevice = nullptr;
    hResult = m_d3dDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
    //CHECK_HRESULT(hResult);

    IDXGIAdapter* dxgiAdapter = nullptr;
    hResult = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter));

    IDXGIFactory* dxgiFactory = nullptr;
    hResult = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgiFactory));

    hResult = dxgiFactory->CreateSwapChain(m_d3dDevice, &sd, &m_SwapChain);

    ReleaseCOM(dxgiDevice);
    ReleaseCOM(dxgiAdapter);
    ReleaseCOM(dxgiFactory);

    // Release the old views, as they hold references to the buffers we
    // will be destroying.  Also release the old depth/stencil buffer.
    //	ReleaseCOM(mRenderTargetView);
    //	ReleaseCOM(mDepthStencilView);
    //	ReleaseCOM(mDepthStencilBuffer);

    // Resize the swap chain and recreate the render target view.
    hResult = m_SwapChain->ResizeBuffers(1, m_Width, m_Height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
    ID3D11Texture2D* backBuffer;
    hResult = m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
    hResult = m_d3dDevice->CreateRenderTargetView(backBuffer, nullptr, &m_BackBufferRT);

    D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
    uavDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
    uavDesc.Texture2D.MipSlice = 0;

    hResult = g_D3Device->CreateUnorderedAccessView(backBuffer, &uavDesc, &m_BackBufferUAV);
    CHECK_HRESULT(hResult);

    ReleaseCOM(backBuffer);

    // Create the depth/stencil buffer and view.
    D3D11_TEXTURE2D_DESC depthStencilDesc{};

    depthStencilDesc.Width = m_Width;
    depthStencilDesc.Height = m_Height;
    depthStencilDesc.MipLevels = 1;
    depthStencilDesc.ArraySize = 1;
    depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilDesc.SampleDesc.Count = MSAA_SAMPLES_COUNT;
    depthStencilDesc.SampleDesc.Quality = m4xMsaaQuality - 1;
    depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
    depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthStencilDesc.CPUAccessFlags = 0;
    depthStencilDesc.MiscFlags = 0;

    hResult = m_d3dDevice->CreateTexture2D(&depthStencilDesc, nullptr, &m_DepthStencilBuffer);
    CHECK_HRESULT(hResult);

    hResult = m_d3dDevice->CreateDepthStencilView(m_DepthStencilBuffer, nullptr, &m_DepthStencilView);
    CHECK_HRESULT(hResult);

    // Bind the render target view and depth/stencil view to the pipeline.
    m_D3DContext->OMSetRenderTargets(1, &m_BackBufferRT, m_DepthStencilView);

    // Set the viewport transform.
    m_ScreenViewport.TopLeftX = 0;
    m_ScreenViewport.TopLeftY = 0;
    m_ScreenViewport.Width = static_cast<float>(SCREEN_WIDTH);
    m_ScreenViewport.Height = static_cast<float>(SCREEN_HEIGHT);
    m_ScreenViewport.MinDepth = 0.0f;
    m_ScreenViewport.MaxDepth = 1.0f;

    m_D3DContext->RSSetViewports(1, &m_ScreenViewport);

    return true;
}

int BaseDX11Application::Run() {
    while (true)
    {
        // Handle windows messages
        MSG msg;
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        if (msg.message == WM_QUIT)
            break;

        Render();
    }

    return 0;
}

void BaseDX11Application::Render()
{

}
