#include "EngineDXApplication.h"

EngineDXApplication::EngineDXApplication(HINSTANCE _hInstance, int _width, int _height)
	:BaseDX11Application(_hInstance, _width, _height),
	m_TempSceneMesh(nullptr)
{
}

EngineDXApplication::~EngineDXApplication()
{
}

void EngineDXApplication::Ugly_Initialize()
{
	// todo: this is UGLY intentionaly and will be removed
	m_txaaMaterial = new DXTXAAMaterial();
	m_FSQMesh = new Mesh(m_FSQGeometry, m_txaaMaterial);
	//m_SkyRenderer.Initialize();
	m_AutomaticExposure = new AutomaticExposureEffect();
}

void EngineDXApplication::Render() {
	color clearColor(1.f, 1.f, 1.f, 1.f);
	color hdrClearColor = SUN_LIGHT_INTENSITY * clearColor;

	/*
	* todo: sky box renderer works wrong
	m_D3DContext->OMSetRenderTargets(1, &m_BackBufferRT, m_DepthStencilView);
	m_D3DContext->ClearRenderTargetView(m_BackBufferRT, &clearColor[0]);
	m_D3DContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// render skybox
	m_SkyRenderer.Render(m_D3DContext);
	m_SwapChain->Present(0, 0);
	return;
	*/

	{
		m_PerfomanceAnotator->BeginEvent(L"Render to OffScreen texture");

		// first render to temp offscreen texture
		auto offscreenRenderTarget = m_txaaMaterial->getCurrentFrameRenderTargetView();
		auto offscreenLinearDepthView = m_txaaMaterial->getCurrentLinearDepthView();

		ID3D11RenderTargetView* firstPassViews[] = { offscreenRenderTarget, offscreenLinearDepthView };
		m_D3DContext->OMSetRenderTargets(2, firstPassViews, m_DepthStencilView);
		m_D3DContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		m_D3DContext->ClearRenderTargetView(offscreenRenderTarget, &hdrClearColor[0]);

		// copy accumulated to current frame srv
		m_D3DContext->CopyResource(m_txaaMaterial->getPrevFrameTexture2D(), m_txaaMaterial->getTempTexture2D());

		if (m_TempSceneMesh != nullptr)
			m_TempSceneMesh->Render(m_D3DContext);

		m_PerfomanceAnotator->EndEvent();
	}

	/*
	// TXAA next render full screen quad
	{
	m_PerfomanceAnotator->BeginEvent(L"Full Screen Quad with TXAA");


	ID3D11RenderTargetView* views[] = { m_BackBufferRT, m_txaaMaterial->getTempRenderTargetView() };
	m_D3DContext->OMSetRenderTargets(2, views, m_DepthStencilView);
	m_D3DContext->ClearRenderTargetView(m_BackBufferRT, &clearColor[0]);
	m_D3DContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//
	m_FSQMesh->Render(m_D3DContext);
	m_PerfomanceAnotator->EndEvent();
	}
	*/

	// generate mips
	{
		m_PerfomanceAnotator->BeginEvent(L"Generate mips");

		// get single pixel with frame color
		m_D3DContext->GenerateMips(m_txaaMaterial->getCurrentFrameSRV());
		m_D3DContext->GenerateMips(m_txaaMaterial->getPrevFrameSRV());

		m_PerfomanceAnotator->EndEvent();
	}

	// automatic exposure
	{
		m_PerfomanceAnotator->BeginEvent(L"Compute shader");

		m_D3DContext->OMSetRenderTargets(0, nullptr, m_DepthStencilView);
		m_AutomaticExposure->Dispatch(m_D3DContext, m_txaaMaterial->getCurrentFrameSRV(), m_BackBufferUAV);

		m_PerfomanceAnotator->EndEvent();
	}

	m_txaaMaterial->SwapFrameTextures();
	g_Camera.setPrevViewProjection(g_Camera.getViewProjection());

	// todo: jiterring commented out
	//g_Camera.nextJitterSample();

	m_SwapChain->Present(0, 0);
}