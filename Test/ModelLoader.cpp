﻿#include "ModelLoader.h"

#include <iostream>
#include <fstream>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <codecvt>

#include "DXDiffuseMaterial.h"
#include "DXTexture.h"

using namespace std;

template<typename VertexData, typename IndexType>
void SaveGeometry(std::string filename, const Mesh* mesh) {
	ofstream fout(filename, ios::out | ios::binary);

	size_t vertexBufferSize = mesh->getVertexBufferSize();
	size_t indexBufferSize = mesh->getIndexBufferSize();
	
    FileHeader fileHeader {
		vertexBufferSize,	// vertex buffer size
		sizeof(FileHeader), // vertex buffer offset
		indexBufferSize,// index buffer size
		sizeof(FileHeader) + vertexBufferSize, // index buffer offset
        mesh->subMeshes.size(),
	};

	// write format structure
	fout.write(reinterpret_cast<const char*>(&fileHeader), sizeof(FileHeader));
	// write vertices data
	fout.write(reinterpret_cast<const char*>(mesh->getVertexBufferPtr()), vertexBufferSize);
	// write indices data
	fout.write(reinterpret_cast<const char*>(mesh->getIndexBufferPtr()), indexBufferSize);

	fout.close();
}

/*
template<typename VertexData, typename IndexType>
shared_ptr<LegacyMesh> LoadGeometry(std::string filename) {
	vector<VertexData> verticesData;
	vector<IndexType> indicesData;

	FileHeader fileHeader;
	ifstream inputFile;
	inputFile.open(filename, ios::binary);

	// read header data as a block:
	inputFile.read(&fileHeader, sizeof(FileHeader));
	inputFile.close();

	return MeshManager::registerMesh(meshId, vertices, indices);
}
*/

Mesh* ModelLoader::LoadModel(const string& modelFilePath)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(modelFilePath, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		cout << "ERROR::ASSIMP::" << importer.GetErrorString() << endl;
		return nullptr;
	}
    
    DefaultGeometry* geometry = new DefaultGeometry("temp");
    Mesh* mesh = new Mesh(*geometry, nullptr);

    // Fill materials
    std::vector<DXDiffuseMaterial*> materials;
    FX11ShaderEffect* effect = new FX11ShaderEffect(L"effects/color.fx");
    processMaterials(effect, scene, materials);
	
    processNode(mesh, scene->mRootNode, scene, materials);

    //SaveGeometry<VertexData, IndexData>(std::string("temp.mesh"), mesh);
	return mesh;
}

// string conversion
std::wstring wStrFromCStr(const char* cStr) {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> convert;
    return convert.from_bytes(cStr);
}

void ModelLoader::processMaterials(FX11ShaderEffect* effect, const aiScene* scene, std::vector<DXDiffuseMaterial*>& materials)
{
    if (!scene->HasMaterials())
        return;

    for (size_t i = 0; i < scene->mNumMaterials; ++i)
    {
        const aiMaterial* material = scene->mMaterials[i];
        aiString texturePath;

        DXDiffuseMaterial* mat = new DXDiffuseMaterial(effect);
        materials.push_back(mat);

        unsigned int numTextures = material->GetTextureCount(aiTextureType_DIFFUSE);

        if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == AI_SUCCESS) {
                wstring path = L"d:\\!Mine\\coredx11\\assets\\";
                path.append(wStrFromCStr(texturePath.C_Str()));
                mat->SetDiffuceMapSRV(DXTexture::LoadTgaTexture(g_D3Device, path.c_str()));
            }
        }
    }
}

void ModelLoader::processNode(Mesh* parent, aiNode* node, const aiScene* scene, std::vector<DXDiffuseMaterial*>& materials)
{
	// Process meshes
	for (unsigned int i = 0; i < node->mNumMeshes; i++) 	{
		aiMesh* mesh_ = scene->mMeshes[node->mMeshes[i]];
		
        auto meshGeometry = new DefaultGeometry(std::string(node->mName.C_Str()));
        processMesh(*meshGeometry, mesh_, scene);
        Mesh* mesh = new Mesh(*meshGeometry, materials[mesh_->mMaterialIndex]);

        parent->subMeshes.push_back(mesh);
	}

	// Recursively process child nodes
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(parent, node->mChildren[i], scene, materials);
	}
}

void ModelLoader::processMesh(DefaultGeometry& geometry, aiMesh* aiMesh_, const aiScene* scene)
{
	// Process vertex data
	for (unsigned int i = 0; i < aiMesh_->mNumVertices; ++i) {
		VertexData vertex{
			// position
			glm::vec3{aiMesh_->mVertices[i].x, aiMesh_->mVertices[i].y, aiMesh_->mVertices[i].z},
			// color
			glm::vec4{1.0f, 1.0f, 1.0f, 1.0f},
			// texture coordinates (uvs)
			glm::vec2{aiMesh_->mTextureCoords[0][i].x, aiMesh_->mTextureCoords[0][i].y}
		};
		// normal
		if(aiMesh_->mNormals != nullptr)
			vertex.Normal = glm::vec3{ aiMesh_->mNormals[i].x, aiMesh_->mNormals[i].y, aiMesh_->mNormals[i].z };
		
		geometry.vertexBuffer.push_back(vertex);
	}

    geometry.setTrienglesCount(aiMesh_->mNumFaces * 3); // temp: triangulated face must have 3 indices for each face
	// Process indices
	for (unsigned int i = 0; i < aiMesh_->mNumFaces; ++i) {
		aiFace face = aiMesh_->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			geometry.indexBuffer.push_back(face.mIndices[j]);
	}
}
