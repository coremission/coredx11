// Nonnumeric values cannot be added to a cbuffer.
//Texture2DMS<float4> gScreenTexture;
Texture2D gDepthTexture;
Texture2D gCurrentFrameTexture;
Texture2D gHistoryBuffer;

// Use linear filtering for minification, magnification, and mipmapping.
SamplerState linearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = WRAP;
    AddressV = WRAP;
};

SamplerState pointSampler
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = CLAMP;
    AddressV = CLAMP;
};

cbuffer cbPerObject
{
    float4x4 gPrevViewProjection;
    float4x4 gCurrentViewProjection;
    float4x4 gCurrentInverseViewProjection;
    float2   gJitterUV;
    float2   gScreenTexelSize;
};

struct VertexIn
{
    float3 PosL   : POSITION;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};

struct VertexOut
{
    float4 PosH   : SV_POSITION;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};


struct PixelOut
{
    float4 Color  : SV_Target0;
    float4 Color2 : SV_Target1;
};

VertexOut VS(VertexIn vin)
{
    VertexOut vout;
    vout.PosH = float4(vin.PosL, 1.0f);
    vout.Color = vin.Color;
    vout.UV = vin.UV;
    vout.Normal = vin.Normal;
    return vout;
}

PixelOut PS(VertexOut pin)
{
    PixelOut result;
    /*result.Color = gCurrentFrameTexture.Sample(linearSampler, pin.UV);
    result.Color2 = result.Color;
    return result;
*/
    // todo: this is for ms textures
    //return gCurrentFrameTexture.Load(int2(100, 100), 0);

    // Reprojection algorithm
    // 1. Find world position of a fragment using Frustum values and linear depth
    
    // 2. Using PrevVP on (1) find coordinates of a fragment on N-1 frame
    /// 2.1 Which must be remapped from -1..1 to UV coordinates (0..1)

    // 3. Do mix magic :)

    // reconstruct worldPos
    //float4 worldPos = float4(pin.UV, 1.0f, 1.0f) * gDepthTexture.Sample(linearSampler, pin.UV);
    //worldPos = mul(worldPos, gCurrentInverseViewProjection);
    float4 worldPos = float4(gDepthTexture.Sample(linearSampler, pin.UV).rgb, 1.0f);

    float4 prevProj = mul(worldPos, gPrevViewProjection);
    prevProj = 0.5f * (prevProj / prevProj.w) + 0.5f;
    prevProj.y = 1 - prevProj.y;
    
    float4 color = gCurrentFrameTexture.Sample(linearSampler, pin.UV - gJitterUV);
    float4 prevColor = gHistoryBuffer.Sample(linearSampler, prevProj.xy);

    // Neighboor clampinb 3x3
    float2 du = float2(gScreenTexelSize.x, 0.0f);
    float2 dv = float2(0.0f, gScreenTexelSize.y);

    float4 ctl = gCurrentFrameTexture.Sample(pointSampler, pin.UV - dv - du);
    float4 ctc = gCurrentFrameTexture.Sample(pointSampler, pin.UV - dv);
    float4 ctr = gCurrentFrameTexture.Sample(pointSampler, pin.UV - dv + du);
    float4 cml = gCurrentFrameTexture.Sample(pointSampler, pin.UV - du);
    float4 cmc = gCurrentFrameTexture.Sample(pointSampler, pin.UV);
    float4 cmr = gCurrentFrameTexture.Sample(pointSampler, pin.UV + du);
    float4 cbl = gCurrentFrameTexture.Sample(pointSampler, pin.UV + dv - du);
    float4 cbc = gCurrentFrameTexture.Sample(pointSampler, pin.UV + dv);
    float4 cbr = gCurrentFrameTexture.Sample(pointSampler, pin.UV + dv + du);

    float4 cmin = min(ctl, min(ctc, min(ctr, min(cml, min(cmc, min(cmr, min(cbl, min(cbc, cbr))))))));
    float4 cmax = max(ctl, max(ctc, max(ctr, max(cml, max(cmc, max(cmr, max(cbl, max(cbc, cbr))))))));

    prevColor = clamp(prevColor, cmin, cmax);

    float4 resultColor = lerp(color, prevColor, 0.834f); // worldPos;

    result.Color = resultColor;
    result.Color2 = resultColor;

    return result;
}

RasterizerState Wireframe
{
    FillMode = WireFrame;
    CullMode = NONE;
};

RasterizerState SolidState
{
    FillMode = Solid;
    CullMode = NONE;
};

float4 SolidColorPS(VertexOut pin) : SV_Target
{
    return float4(0.0f, 0.0f, 1.0f, 1.0f);
}

technique11 Default
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_5_0, VS()));
        SetGeometryShader(NULL);
        SetRasterizerState(SolidState);
        SetPixelShader(CompileShader(ps_5_0, PS()));
    }
}

// Here some useful snippets
// todo: this is for grayscale
//float lum = c.r*.3 + c.g*.59 + c.b*.11;
//return float4(lum, lum, lum, 1.0f);