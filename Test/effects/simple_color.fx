// Nonnumeric values cannot be added to a cbuffer.
Texture2D gTextureMap;

// Use linear filtering for minification, magnification, and mipmapping.
SamplerState linearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = WRAP;
    AddressV = WRAP;
};

struct VertexIn
{
    float3 PosL   : POSITION;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};

struct VertexOut
{
    float4 PosH   : SV_POSITION;
    float2 UV     : TEXTCOORD;
};

struct PixelOut
{
    float4 Color       : SV_Target0;
};

VertexOut VS(VertexIn vin)
{
    VertexOut vout;

    // Transform to homogeneous clip space.
    vout.PosH = float4(vin.PosL, 1.0);
    vout.UV = vin.UV;
    return vout;
}

PixelOut PS(VertexOut pin)
{
    PixelOut result;
    result.Color = gTextureMap.Sample(linearSampler, pin.UV);
    return result;
}

RasterizerState SolidState
{
    FillMode = Solid;
    CullMode = FRONT;
};

technique11 Default
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_5_0, VS()));
        SetGeometryShader(NULL);
        SetRasterizerState(SolidState);
        SetPixelShader(CompileShader(ps_5_0, PS()));
    }
}
