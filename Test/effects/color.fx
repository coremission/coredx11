// Nonnumeric values cannot be added to a cbuffer.
Texture2D gDiffuseMap;

// Use linear filtering for minification, magnification, and mipmapping.
SamplerState linearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = WRAP;
    AddressV = WRAP;
};

cbuffer cbPerObject
{
    float4x4 gWorldViewProj;
    float3   gCameraPos;
    float    gSunIntensity;
};

struct VertexIn
{
    float3 PosL   : POSITION;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};

struct VertexOut
{
    float4 PosH   : SV_POSITION;
    float3 PosW   : POSITION;
    float  Depth  : DEPTH;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};

struct PixelOut
{
    float4 Color       : SV_Target0;
    float4 LinearDepth : SV_Target1;
};

VertexOut VS(VertexIn vin)
{
    VertexOut vout;

    // Transform to homogeneous clip space.
    vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);
    vout.Depth = vout.PosH.z;
    vout.PosW = vin.PosL;
    vout.Color = vin.Color;
    vout.UV = vin.UV;
    vout.Normal = vin.Normal;
    return vout;
}

PixelOut PS(VertexOut pin)
{
    PixelOut result;
    float3 lightDir = normalize(float3(0.5f, 0.5f, 0.5f));

    float4 texColor = gDiffuseMap.Sample(linearSampler, pin.UV);
    float halfLambert = 0.5f * (dot(normalize(pin.Normal), lightDir) + 1);

    result.Color = gSunIntensity * texColor * halfLambert; // world pos as color float4(pin.PosW, 1.0f);
    result.LinearDepth = float4(pin.PosW, pin.Depth);
    return result;
}

RasterizerState Wireframe
{
    FillMode = WireFrame;
    CullMode = NONE;
};

RasterizerState SolidState
{
    FillMode = Solid;
    CullMode = FRONT;
};

float4 SolidColorPS(VertexOut pin) : SV_Target
{
    return float4(0.0f, 0.0f, 0.0f, 1.0f);
}

technique11 Default
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_5_0, VS()));
        SetGeometryShader(NULL);
        SetRasterizerState(SolidState);
        SetPixelShader(CompileShader(ps_5_0, PS()));
    }

    /*
    pass P1
    {
        SetVertexShader(CompileShader(vs_5_0, VS()));
        SetRasterizerState(Wireframe);
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_5_0, SolidColorPS()));
    }
    */
}
