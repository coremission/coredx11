// Nonnumeric values cannot be added to a cbuffer.
TextureCube gCubeMap;

cbuffer cbPerObject
{
    float4x4 gOrientation;
};

SamplerState linearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = WRAP;
    AddressV = WRAP;
};

struct VertexIn
{
    float3 PosL   : POSITION;
    float4 Color  : COLOR;
    float2 UV     : TEXTCOORD;
    float3 Normal : NORMAL;
};

struct VertexOut
{
    float4 PosH   : SV_POSITION;
    float3 UVW    : POSITION;
};

VertexOut VS(VertexIn vin)
{
    VertexOut vout;
    //vout.PosH = float4(vin.PosL, 1.0f);
    vout.PosH = mul(float4(vin.PosL, 1.0f), gOrientation);
    vout.UVW = vin.PosL;
    //vout.UVW = mul(float4(vin.PosL, 1.0f), gOrientation).xyz;

    return vout;
}

float4 PS(VertexOut pin): SV_TARGET
{
    //return float4(normalize(pin.UVW), 1.0f);
    return gCubeMap.Sample(linearSampler, normalize(pin.UVW));
}

RasterizerState SolidState
{
    FillMode = Solid;
    CullMode = NONE;
    DepthClipEnable = false;
};

technique11 Default
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_5_0, VS()));
        SetGeometryShader(NULL);
        SetRasterizerState(SolidState);
        SetPixelShader(CompileShader(ps_5_0, PS()));
    }
}
