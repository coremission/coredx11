#include "frostbite.fx"

Texture2D gInput;
RWTexture2D<float4> gOutput;

cbuffer constants{
    float adaptationSpeed;
};

SamplerState pointSampler
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};


// P0. Adaptation pass (apply current color to accumulation buffer)
RWTexture2D<float> gAccumulateLuminanceTexture;

[numthreads(1, 1, 1)]
void Adapt(int3 groupThreadID : SV_GroupThreadID, int3 dispatchThreadID : SV_DispatchThreadID)
{
    // 1. Here meter current frame average Luminance
    float4 frameColor = gInput.SampleLevel(pointSampler, float2(0, 0), 9); // sample from 9th mip level 1x1 size
    float Lnew = 0.2126f * frameColor.r + 0.7152f * frameColor.g + 0.0722 * frameColor.b;

    // todo: dumb transition
    //gAccumulateLuminanceTexture[dispatchThreadID.xy] = lerp(gAccumulateLuminanceTexture[dispatchThreadID.xy], currentLum, adaptationSpeed);
    float Lavg = gAccumulateLuminanceTexture[dispatchThreadID.xy];
    float speed = 1.0f; // todo: here must be different adaptation speeds for light-dark, dark-light;

    // todo: for instant application
    //Lavg = Lavg + (Lnew - Lavg) * (1 - exp(-0.016f * speed));
    Lavg = Lnew;

    gAccumulateLuminanceTexture[dispatchThreadID.xy] = Lavg;
}

// P1. Applying auto exposure

float4 toneExposure(float4 vColor, float average)
{
    float T = pow(average, -1);

    float4 result;
    result.r = 1 - exp(-T * vColor.r);
    result.g = 1 - exp(-T * vColor.g);
    result.b = 1 - exp(-T * vColor.b);
    result.a = vColor.a;

    return result;
}

float4 toneDefault(float4 vColor, float average)
{
    float g_fMaxLuminance = 0.9f;
    float g_fMiddleGrey = 0.5f;
    float4 LUM_CONVERT = float4(0.2126f, 0.7152f, 0.0722, 0);

    float fLumAvg = exp(average);

    // Calculate the luminance of the current pixel
    float fLumPixel = dot(vColor, LUM_CONVERT);

    return float4(fLumPixel, fLumPixel, fLumPixel, 1000) * 0.001f;

    // Apply the modified operator (Eq. 4)
    float fLumScaled = (fLumPixel * g_fMiddleGrey) / fLumAvg;
    float fLumCompressed = (fLumScaled * (1 + (fLumScaled / (g_fMaxLuminance * g_fMaxLuminance)))) / (1 + fLumScaled);
    return fLumCompressed * vColor;
}

[numthreads(1, 1, 1)]
void ApplyAutoExposure(int3 groupThreadID : SV_GroupThreadID, int3 dispatchThreadID : SV_DispatchThreadID)
{
    float currentLum = gAccumulateLuminanceTexture[int2(0, 0)]; // sample from accumulation texture
    float4 initialColor = gInput[dispatchThreadID.xy];
    //float4 adaptedColor = toneExposure(gInput[dispatchThreadID.xy], currentLum);
    float4 adaptedColor = toneDefault(gInput[dispatchThreadID.xy], currentLum);

    float4 resultColor = (dispatchThreadID.x > 400) ? (initialColor / 1000.0f) : adaptedColor;
    gOutput[dispatchThreadID.xy] = resultColor;
}

technique11 Default
{
    pass P0
    {
        SetVertexShader(NULL);
        SetPixelShader(NULL);
        SetComputeShader(CompileShader(cs_5_0, Adapt()));
    }

    pass P1
    {
        SetVertexShader(NULL);
        SetPixelShader(NULL);
        SetComputeShader(CompileShader(cs_5_0, ApplyAutoExposure()));
    }
}