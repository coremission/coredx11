﻿#include <utility>
#include "DXTXAAMaterial.h"
#include "render.hpp"

DXTXAAMaterial::DXTXAAMaterial()
    : DXMaterial(new FX11ShaderEffect(L"effects/txaa.fx")),
    m_FirstTexture(DXTexture::Create(SCREEN_WIDTH, SCREEN_HEIGHT, MSAA_SAMPLES_COUNT, 1)),
    m_SecondTexture(DXTexture::Create(SCREEN_WIDTH, SCREEN_HEIGHT, MSAA_SAMPLES_COUNT, 1)),
    m_TempTexture(DXTexture::Create(SCREEN_WIDTH, SCREEN_HEIGHT, MSAA_SAMPLES_COUNT, 1)),
    m_FirstLinearDepth(DXTexture::Create(SCREEN_WIDTH, SCREEN_HEIGHT, MSAA_SAMPLES_COUNT)),
    m_SecondLinearDepth(DXTexture::Create(SCREEN_WIDTH, SCREEN_HEIGHT, MSAA_SAMPLES_COUNT))
{
    m_CurrentFrameTexture = m_Effect->getVariableByName("gCurrentFrameTexture")->AsShaderResource();
    m_HistoryBufferTexture = m_Effect->getVariableByName("gHistoryBuffer")->AsShaderResource();
    m_DepthTexture = m_Effect->getVariableByName("gDepthTexture")->AsShaderResource();

    m_PrevVP = m_Effect->getVariableByName("gPrevViewProjection")->AsMatrix();
    m_CurrentVP = m_Effect->getVariableByName("gCurrentViewProjection")->AsMatrix();
    m_CurrentInverseVP = m_Effect->getVariableByName("gCurrentInverseViewProjection")->AsMatrix();
    m_JitterUV = m_Effect->getVariableByName("gJitterUV")->AsVector();
    m_ScreenTexelSize = m_Effect->getVariableByName("gScreenTexelSize")->AsVector();

    currentFrameTexture = &m_FirstTexture;
    prevFrameTexture = &m_SecondTexture;
    currentFrameLinearDepth = &m_FirstLinearDepth;
    prevFrameLinearDepth = &m_SecondLinearDepth;
}

void DXTXAAMaterial::SwapFrameTextures()
{
    std::swap(currentFrameTexture, prevFrameTexture);
    std::swap(currentFrameLinearDepth, prevFrameLinearDepth);
}

void DXTXAAMaterial::RefreshMaterialValues()
{
    m_CurrentFrameTexture->SetResource(getCurrentFrameSRV());
    m_HistoryBufferTexture->SetResource(getPrevFrameSRV());
    m_DepthTexture->SetResource(getCurrentLinearDepthSRV());

    glm::mat4 prevViewProjection = g_Camera.getPrevViewProjection();
    m_PrevVP->SetMatrix(reinterpret_cast<float*>(&prevViewProjection[0][0]));

    glm::mat4 currentViewProjection = g_Camera.getViewProjection();
    m_CurrentVP->SetMatrix(reinterpret_cast<float*>(&currentViewProjection[0][0]));

    glm::mat4 currentInverseViewProjection = glm::inverse(currentViewProjection);
    m_CurrentInverseVP->SetMatrix(reinterpret_cast<float*>(&currentInverseViewProjection[0][0]));

    glm::vec2 jitterUV = g_Camera.getJitterUVs();
    m_JitterUV->SetFloatVector(&jitterUV[0]);

    glm::vec2 screenTexelSize = glm::vec2(1 / float(SCREEN_WIDTH), 1 / float(SCREEN_HEIGHT));
    m_ScreenTexelSize->SetFloatVector(&screenTexelSize[0]);
}

