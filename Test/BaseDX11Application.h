#pragma once

#include <windows.h>
#include <d3d11.h>
#include "mesh.h"
#include "DXTexture.h"
#include "FullScreenQuad.h"
#include "DXTXAAMaterial.h"
#include "SkyboxRenderer.h"
#include "AutomaticExposureEffect.h"

class BaseDX11Application {
protected:
    HINSTANCE hInstance;
    HWND hWnd;

    ID3D11Device* m_d3dDevice;
    ID3D11DeviceContext* m_D3DContext;
    IDXGISwapChain* m_SwapChain;
    ID3D11Texture2D* m_DepthStencilBuffer;
    ID3D11RenderTargetView* m_BackBufferRT;
    ID3D11UnorderedAccessView* m_BackBufferUAV;

    ID3D11DepthStencilView* m_DepthStencilView;
    D3D11_VIEWPORT m_ScreenViewport;
    ID3DUserDefinedAnnotation* m_PerfomanceAnotator;

    int m_Width;
    int m_Height;

public:
    BaseDX11Application(HINSTANCE _hInstance, int _width, int _height) :
        hInstance(_hInstance),
        m_Width(_width),
        m_Height(_height)
    {}
    virtual ~BaseDX11Application() {}

    LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
    bool CreateGameWindow();
    bool Init();
    int Run();
    virtual void Render();
};
