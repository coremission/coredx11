﻿#include "Transform.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/euler_angles.inl>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

using namespace glm;

Transform::Transform():
	localPosition(vec3()),
	localScale(vec3(1, 1, 1)),
	localRotation(vec3()),
	parent(nullptr)
{
}

Transform::~Transform()
{
}

void Transform::recalculateMatrices() const
{
    // Original formula for OpenGl
    // transformedVector = TranslationMatrix * RotationMatrix * ScaleMatrix * originalVector;

	//auto result = translate(localPosition) * glm::mat4_cast(localRotation) * scale(localScale);
    mat4 Translation = translate(localPosition);
    mat4 Rotation = toMat4(localRotation);
    mat4 Scale = scale(localScale);

    auto result = Scale * Rotation * Translation;
    //todo: this is with right translation: auto result = Translation * Rotation * Scale;
    
    // transpose result for DirectX
    //result = transpose(result);

	if (parent != nullptr)
		result = parent->localToWorldMatrix * result;

	if (parent != nullptr)
		rotation = parent->getRotation() * localRotation;
	else
		rotation = localRotation;

	localToWorldMatrix = result;
	worldToLocalMatrix = glm::inverse(localToWorldMatrix);
}

void Transform::setLocalPosition(vec3 value)
{
	localPosition = value;
	recalculateMatrices();
}

glm::vec3 Transform::getPosition() const
{
	return transformPoint(vec3(0, 0, 0));
}

glm::quat Transform::getRotation() const
{
	recalculateMatrices();
	return rotation;
}

void Transform::setLocalRotation(glm::quat value)
{
	localRotation = value;
	recalculateMatrices();
}

void Transform::setLocalYawPitchRoll(glm::vec3 yawPitchRoll)
{
	localRotation = quat(yawPitchRoll);
	recalculateMatrices();
}

void Transform::rotate(glm::vec3 yawPitchRoll)
{
	localRotation = localRotation * quat(yawPitchRoll);
	recalculateMatrices();
}

void Transform::lookAt(glm::vec3 targetWorldPos)
{
	auto eyeWorldPos = transformPoint(vec3(0, 0, 0));

	std::cout << eyeWorldPos.x << eyeWorldPos.y << eyeWorldPos.z;
	auto lookQuaternion = quat_cast(glm::lookAt(eyeWorldPos, targetWorldPos, vec3(0, 1, 0)));
	setLocalRotation(lookQuaternion);
}

void Transform::setLocalScale(glm::vec3 value)
{
	localScale = value;
	recalculateMatrices();
}

const mat4& Transform::getLocalToWorldMatrix() const
{
	recalculateMatrices();
	return localToWorldMatrix;
}

const glm::mat4& Transform::getWorldToLocalMatrix() const
{
	recalculateMatrices();
	return worldToLocalMatrix;
}

glm::vec3 Transform::transformPoint(glm::vec3 p) const
{
    // OpenGL version
	//return vec3(getLocalToWorldMatrix() * vec4(p, 1.0f));
    return vec3(vec4(p, 1.0f) * getLocalToWorldMatrix());
}

glm::vec3 Transform::inverseTransformPoint(glm::vec3 p)
{
	return vec3(getWorldToLocalMatrix() * vec4(p, 1.0f));
}

glm::vec3 Transform::transformDirection(glm::vec3 direction)
{
    // OpenGL version
	//return vec3(getLocalToWorldMatrix() * vec4(direction, 0));
    return vec3(vec4(direction, 0) * getLocalToWorldMatrix());
}

glm::vec3 Transform::inverseTransformDirection(glm::vec3 direction)
{
	return vec3(getWorldToLocalMatrix() * vec4(direction, 0));
}