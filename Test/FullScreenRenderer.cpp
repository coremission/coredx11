#include "FullScreenRenderer.h"

FullScreenRenderer::FullScreenRenderer()
{
	m_FSQMesh = new Mesh(m_FSQGeometry);
}


FullScreenRenderer::~FullScreenRenderer()
{
}

void FullScreenRenderer::SetMaterial(DXMaterial * material)
{
    m_CurrentMaterial = material;
    m_FSQMesh->setMaterial(m_CurrentMaterial);
}

void FullScreenRenderer::Render()
{
	// render full screen quad
    m_FSQMesh->Render(g_D3DContext);
}
