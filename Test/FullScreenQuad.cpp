﻿#include "FullScreenQuad.h"
#include "render.hpp"

FullScreenQuad::FullScreenQuad()
    : Geometry<Vertex>("full_screen_quad")
{
    vertexBuffer =
    {
        //Pos, Color, UVs, Normal
        { glm::vec3(-1.0f, -1.0f, 0.0f), Colors::White, glm::vec2(0.0f, 1.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(-1.0f, +1.0f, 0.0f), Colors::White, glm::vec2(0.0f, 0.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(+1.0f, +1.0f, 0.0f), Colors::White, glm::vec2(1.0f, 0.0f), glm::vec3(0, 0, 1) },
        { glm::vec3(+1.0f, -1.0f, 0.0f), Colors::White, glm::vec2(1.0f, 1.0f), glm::vec3(0, 0, 1) },
    };

    indexBuffer = {
        // front face
        0, 1, 2,
        0, 2, 3,
    };
    m_TrianglesCount = 6;
}
