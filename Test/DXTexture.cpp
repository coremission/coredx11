#include "DXTexture.h"
#include "render.hpp"
#include "helpers.hpp"
#include "DirectXTex.h"
#include <comdef.h>
#include <iostream>

DXTexture::DXTexture()
    :m_TextureSRV(nullptr),
    m_Texture2D(nullptr),
    m_RenderTargetView(nullptr)
{}

DXTexture::DXTexture(const DXTexture& texture)
{
    this->m_RenderTargetView = texture.m_RenderTargetView;
    this->m_Texture2D = texture.m_Texture2D;
    this->m_TextureSRV = texture.m_TextureSRV;
    this->m_TextureUAV = texture.m_TextureUAV;
    this->m_DepthStencilView = texture.m_DepthStencilView;
}

DXTexture::~DXTexture()
{}

DXTexture DXTexture::Create(size_t width, size_t height, size_t samplesCount, size_t mipLevelsCount, DXGI_FORMAT format, UINT bindFlags)
{
    DXTexture result;

    // 1. Create Texture2D
    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof(textureDesc));
    textureDesc.Width = width;
    textureDesc.Height = height;
    textureDesc.MipLevels = 0;
    textureDesc.ArraySize = 1;
    textureDesc.Format = format;
    textureDesc.SampleDesc.Count = samplesCount;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = bindFlags;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

    HRESULT hResult = g_D3Device->CreateTexture2D(&textureDesc, nullptr, &result.m_Texture2D);
    CHECK_HRESULT(hResult);

    // 2. Create Render Target View (or Depth Stencil View)
    /*
    D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    renderTargetViewDesc.Format = DXGI_FORMAT_UNKNOWN;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D.MipSlice = 0;
    */
    hResult = g_D3Device->CreateRenderTargetView(result.m_Texture2D, nullptr /* todo: why this is not work? &renderTargetViewDesc */, &result.m_RenderTargetView);
    CHECK_HRESULT(hResult);

    // 3. Create SRV
    if ((bindFlags & D3D11_BIND_SHADER_RESOURCE) != 0)
    {
        D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
        shaderResourceViewDesc.Format = DXGI_FORMAT_UNKNOWN;
        shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
        shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
        shaderResourceViewDesc.Texture2D.MipLevels = UINT_MAX;

        hResult = g_D3Device->CreateShaderResourceView(result.m_Texture2D, &shaderResourceViewDesc, &result.m_TextureSRV);
        CHECK_HRESULT(hResult);
    }

    // 4. Create UAV
    if ((bindFlags & D3D11_BIND_UNORDERED_ACCESS) != 0)
    {
        D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
        uavDesc.Format = format;
        uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
        uavDesc.Texture2D.MipSlice = 0;

        hResult = g_D3Device->CreateUnorderedAccessView(result.m_Texture2D, &uavDesc, &result.m_TextureUAV);
        CHECK_HRESULT(hResult);
    }

    return result;
}

DXTexture DXTexture::CreateDepthStencil(size_t width, size_t height, size_t samplesCount)
{
    DXTexture result;

    // 1. Create Texture2D
    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof(textureDesc));
    textureDesc.Width = width;
    textureDesc.Height = height;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
    textureDesc.SampleDesc.Count = samplesCount;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    HRESULT hResult = g_D3Device->CreateTexture2D(&textureDesc, nullptr, &result.m_Texture2D);
    CHECK_HRESULT(hResult);

    // 2. Create Depth Stencil View
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDescription;
    depthStencilViewDescription.Flags = 0;
    depthStencilViewDescription.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDescription.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDescription.Texture2D.MipSlice = 0;

    hResult = g_D3Device->CreateDepthStencilView(result.m_Texture2D, &depthStencilViewDescription, &result.m_DepthStencilView);
    CHECK_HRESULT(hResult);

    // 3. Create SRV
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription;
    srvDescription.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
    srvDescription.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDescription.Texture2D.MostDetailedMip = 0;
    srvDescription.Texture2D.MipLevels = 1;

    hResult = g_D3Device->CreateShaderResourceView(result.m_Texture2D, &srvDescription, &result.m_TextureSRV);
    CHECK_HRESULT(hResult);

    return result;
}

ID3D11ShaderResourceView * DXTexture::LoadTgaTexture(ID3D11Device* pDevice, const wchar_t* pSrcFile)
{
    ID3D11Resource* resource = nullptr;
    D3DX11CreateTextureFromFile(pDevice, pSrcFile, true, &resource);
   
    ID3D11Texture2D* texture = static_cast<ID3D11Texture2D*>(resource);

	if (texture == nullptr)
	{
		return nullptr;
	}

    D3D11_SHADER_RESOURCE_VIEW_DESC defaultSRVDesc;
        defaultSRVDesc.Format = DXGI_FORMAT_UNKNOWN;
        defaultSRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
        defaultSRVDesc.Texture2D.MostDetailedMip = 0;
        defaultSRVDesc.Texture2D.MipLevels = UINT_MAX;

    ID3D11ShaderResourceView* shaderResourceView;
    CHECK_HRESULT(pDevice->CreateShaderResourceView(texture, &defaultSRVDesc, &shaderResourceView));

    return shaderResourceView;
}

HRESULT DXTexture::D3DX11CreateTextureFromFile(ID3D11Device* pDevice, const wchar_t* pSrcFile, bool generateMips, ID3D11Resource** ppTexture)
{
    DirectX::ScratchImage image;
    HRESULT hr = S_OK;
    /*if (common::StrEnds(pSrcFile, L".dds", false))
        hr = DirectX::LoadFromDDSFile(pSrcFile, DirectX::DDS_FLAGS_NONE, nullptr, image);
    else if (common::StrEnds(pSrcFile, L".tga", false))
    */    
    hr = DirectX::LoadFromTGAFile(pSrcFile, nullptr, image);
    /*else
        hr = DirectX::LoadFromWICFile(pSrcFile, DirectX::WIC_FLAGS_NONE, nullptr, image);
        */
    
    if (FAILED(hr))
        return hr;

    DirectX::ScratchImage imageWithMipMaps;
    DirectX::ScratchImage* finalImage = &image;
    if (generateMips && image.GetMetadata().mipLevels == 1)
    {
        const DirectX::Image* origImg = image.GetImage(0, 0, 0);
        assert(origImg);
        CHECK_HRESULT(DirectX::GenerateMipMaps(*origImg,
            0, // filter
            0, // levels
            imageWithMipMaps));

        finalImage = &imageWithMipMaps;
    }

    hr = DirectX::CreateTextureEx(
        pDevice,
        finalImage->GetImages(),
        finalImage->GetImageCount(),
        finalImage->GetMetadata(),
        D3D11_USAGE_IMMUTABLE,
        D3D11_BIND_SHADER_RESOURCE,
        0,      // cpuAccessFlags
        0,      // miscFlags
        false,  // forceSRGB
        ppTexture);
    return hr;
}
