#include <windows.h>
#include "EngineDXApplication.h"
#include "PTDXApplication.h"
#include "ModelLoader.h"
#include "render.hpp"
#include <iostream>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
#ifdef PATH_TRACER
	PTDXApplication application(hInstance, SCREEN_WIDTH, SCREEN_HEIGHT);
	if (application.Init()) {
		return application.Run();
	}
#else
	EngineDXApplication application(hInstance, SCREEN_WIDTH, SCREEN_HEIGHT);
	
	if (application.Init()) {
		application.Ugly_Initialize();

		Mesh* scene = ModelLoader::LoadModel(R"(D:\!Mine\coredx11\assets\sponza.obj)");

		application.SetTempMesh(scene);
		return application.Run();
	}
#endif // PATH_TRACER

    std::cerr << "D3D Application couldn't initialized";
    return -1;
}