﻿#pragma once

#include <d3d11.h>

class Cubemap
{
    ID3D11ShaderResourceView* m_CubemapSRV;
public:
    explicit Cubemap();
    void LoadFromFile(const wchar_t* filename);
    ID3D11ShaderResourceView* getCubemapSRV() { return m_CubemapSRV; }
};
