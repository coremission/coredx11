﻿#pragma once

#include <assimp/scene.h>
#include <memory>
#include <vector>
#include "geometry.h"
#include "DXDiffuseMaterial.h"
#include "mesh.h"

struct GeometryFileHeader {
	size_t VertexBufferSize;
	size_t VertexBufferOffset;
	size_t IndexBufferSize;
	size_t IndexBufferOffset;
};

using VertexData = Vertex;
using IndexData = unsigned int;

struct FileHeader {
    size_t VertexBufferSize;
    size_t VertexBufferOffset;
    size_t IndexBufferSize;
    size_t IndexBufferOffset;

    size_t SubMeshesNumber;
    size_t SubMeshesOffsetArray;
};

class ModelLoader
{
private:
    static void processMaterials(FX11ShaderEffect* effect, const aiScene* scene, std::vector<DXDiffuseMaterial*>& materials);
	static void processNode(Mesh* mesh, aiNode* node, const aiScene* scene, std::vector<DXDiffuseMaterial*>& materials);
	static void processMesh(DefaultGeometry& mesh, aiMesh* aiMesh, const aiScene* scene);
public:
	static Mesh* LoadModel(const std::string& filePath);
};
