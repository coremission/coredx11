﻿#include "Cubemap.h"
#include "helpers.hpp"
#include "render.hpp"
#include "DirectXTex.h"

Cubemap::Cubemap()
    : m_CubemapSRV(nullptr)
{}

void Cubemap::LoadFromFile(const wchar_t* filename)
{
    DirectX::ScratchImage image;
    HRESULT hr = S_OK;
    hr = DirectX::LoadFromDDSFile(filename, 0, nullptr, image);
    CHECK_HRESULT(hr);

    ID3D11Resource* cubemapResource = nullptr;
    hr = DirectX::CreateTextureEx(
        g_D3Device,
        image.GetImages(),
        image.GetImageCount(),
        image.GetMetadata(),
        D3D11_USAGE_IMMUTABLE,
        D3D11_BIND_SHADER_RESOURCE,
        0,      // cpuAccessFlags
        0,      // miscFlags
        false,  // forceSRGB
        &cubemapResource);

    CHECK_HRESULT(hr);

    ID3D11Texture2D* cubemapTexture = static_cast<ID3D11Texture2D*>(cubemapResource);

    if (cubemapTexture == nullptr)
    {
        return;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC defaultSRVDesc;
    defaultSRVDesc.Format = DXGI_FORMAT_UNKNOWN;
    defaultSRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
    defaultSRVDesc.Texture2D.MostDetailedMip = 0;
    defaultSRVDesc.Texture2D.MipLevels = UINT_MAX;

    
    hr = g_D3Device->CreateShaderResourceView(cubemapTexture, &defaultSRVDesc, &m_CubemapSRV);
    CHECK_HRESULT(hr);
}
