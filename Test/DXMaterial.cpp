﻿#include "DXMaterial.h"
#include "helpers.hpp"
#include "DXDiffuseMaterial.h"

DXMaterial::DXMaterial(FX11ShaderEffect* effect)
    : m_Effect(effect), m_InputLayout(nullptr)
{
    BuildInputLayout();
}

DXMaterial::~DXMaterial()
{
    ReleaseCOM(m_InputLayout);
}

void DXMaterial::BuildInputLayout()
{
    // Create the vertex input layout.
    D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
    {
        { "POSITION",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR",      0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        // Remember(!) Semantic name can't contain number at the end, next parameter "Semantic index" determines it
        // this is not work { "TEXTCOORD1", 0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXTCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };

    // Create the input layout
    D3DX11_PASS_DESC passDesc = m_Effect->getPassDescription(0); // most materials use only 1 pass
    HRESULT hResult = g_D3Device->CreateInputLayout(vertexDesc, count_of(vertexDesc),
        passDesc.pIAInputSignature,
        passDesc.IAInputSignatureSize, &m_InputLayout);

    CHECK_HRESULT(hResult);
}

void DXMaterial::RefreshMaterialValues()
{
}
