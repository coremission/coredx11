#pragma once

#include "BaseDX11Application.h"

class EngineDXApplication: public BaseDX11Application
{
	// todo: remove
	Mesh* m_TempSceneMesh;
	SkyboxRenderer m_SkyRenderer;
	AutomaticExposureEffect* m_AutomaticExposure;
	// fullscreen temp
	FullScreenQuad m_FSQGeometry;
	Mesh* m_FSQMesh;
	DXTXAAMaterial* m_txaaMaterial;
public:
	EngineDXApplication(HINSTANCE _hInstance, int _width, int _height);
	~EngineDXApplication();

	void SetTempMesh(Mesh* tempMesh) { m_TempSceneMesh = tempMesh; }
	void Ugly_Initialize();

	void Render();
};

