must be
/assets (sponza.obj, sponza.mtl)
/assets/textures

/external/assimp      https://github.com/assimp/assimp/archive/v4.1.0.zip     and the cmake ./
/external/DirectXTex  https://github.com/Microsoft/DirectXTex/archive/may2018.zip
/external/FX11        https://github.com/Microsoft/FX11/archive/may2018.zip
/external/glm         https://github.com/g-truc/glm/archive/0.9.9.0.zip

assimp-vc-140-mt.dll copy to output